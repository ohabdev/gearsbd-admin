import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { CatimagesComponent, BrandCreateComponent, BrandUpdateComponent } from "./catimages.component";

import { CatimageService } from "./service";

import { MediaModule } from "../media/media.module";
import { UtilsModule } from "../utils/utils.module";

const routes: Routes = [
  {
    path: "",
    component: CatimagesComponent,
    data: {
      title: "Manage Catimages",
      urls: [{ title: "Catimages", url: "/catimages" }, { title: "Listing" }],
    },
  },
  {
    path: "create",
    component: BrandCreateComponent,
    data: {
      title: "Manage Catimages",
      urls: [
        { title: "Catimages", url: "/catimages" },
        { title: "Create new banner" },
      ],
    },
  },
  {
    path: "update/:id",
    component: BrandUpdateComponent,
    data: {
      title: "Manage banners",
      urls: [{ title: "Banners", url: "/catimages" }, { title: "Update banner" }],
    },
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgbModule.forRoot(),
    MediaModule,
    RouterModule.forChild(routes),
    UtilsModule
  ],
  declarations: [CatimagesComponent, BrandCreateComponent, BrandUpdateComponent],
  providers: [CatimageService],
  exports: [],
  entryComponents: [],
})
export class CatimageModule { }
