import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';
import { ToastyService } from 'ng2-toasty';
import { CatimageService } from './service';

@Component({
  templateUrl: './catimages.html'
})
export class CatimagesComponent implements OnInit {
  public items = [];
  public page = 1;
  public total = 0;
  public title: string = '';

  constructor(private router: Router, private catimageService: CatimageService, private toasty: ToastyService) {
  }

  ngOnInit() {
    this.query();
  }

  query() {
    this.catimageService.search({
      page: this.page,
      title: this.title
    })
      .then(resp => {
        this.items = resp.data.items;
        this.total = resp.data.count;
      })
      .catch(() => alert('Something went wrong, please try again!'));
  }

  keyPress(event: any) {
    if (event.charCode === 13) {
      this.query();
    }
  }

  remove(item: any, index: number) {
    if (window.confirm('Are you sure want to delete this category image?')) {
      this.catimageService.remove(item._id)
        .then(() => {
          this.toasty.success('Item has been deleted!');
          this.items.splice(index, 1);
        })
        .catch((err) => this.toasty.error(err.data.message || 'Something went wrong, please try again!'));
    }
  }
}

@Component({
  templateUrl: './form.html'
})
export class BrandCreateComponent implements OnInit {
  public catimage: any = {
    name: '',
    alias: '',
    description: '',
    settings: {},
    position: 'default'
  };
  public media: any;

  constructor(private router: Router, private catimageService: CatimageService, private toasty: ToastyService) {
  }

  ngOnInit() { }

  selectMedia(media: any) {
    this.media = media;
  }

  submit(frm: any) {
    if (!this.catimage.title) {
      return this.toasty.error('Please enter Image name');
    }

    if (this.media) {
      this.catimage.mediaId = this.media._id;
    } else {
      return this.toasty.error('Please browse category image');
    }

    this.catimageService.create(this.catimage)
      .then(() => {
        this.toasty.success('Category image has been created');
        this.router.navigate(['/catimages']);
      }, err => this.toasty.error(err.data.message || 'Something went wrong!'));
  }
}

@Component({
  templateUrl: './form.html'
})
export class BrandUpdateComponent implements OnInit {
  public catimage: any;
  public media: any;

  constructor(private router: Router, private route: ActivatedRoute, private CatimageService: CatimageService, private toasty: ToastyService) {
  }

  ngOnInit() {
    let bannerId = this.route.snapshot.paramMap.get('id');
    this.CatimageService.findOne(bannerId)
      .then(resp => {
        this.catimage = resp.data;
        if (resp.data.media) {
          this.media = resp.data.media;
          this.catimage.mediaId = resp.data.media._id;
        }
      });
  }

  selectMedia(media: any) {
    this.media = media;
  }

  submit(frm: any) {
    if (!this.catimage.title) {
      return this.toasty.error('Please enter image name');
    }

    if (this.media) {
      this.catimage.mediaId = this.media._id;
    } else {
      return this.toasty.error('Please browse category image');
    }

    this.CatimageService.update(this.catimage._id, this.catimage)
      .then(() => {
        this.toasty.success('Category image has been updated');
        this.router.navigate(['/catimages']);
      }, err => this.toasty.error(err.data.message || 'Something went wrong!'));
  }
}
