// import { Injectable } from '@angular/core';
// import { RestangularModule, Restangular } from 'ngx-restangular';
// import 'rxjs/add/operator/toPromise';

// @Injectable()
// export class ComplainService {

//   constructor(private restangular: Restangular) { }

//   create(data: any): Promise<any> {
//     return this.restangular.all('complains').post(data).toPromise();
//   }

//   update(id, data: any): Promise<any> {
//     return this.restangular.one('complains', id).customPUT(data).toPromise();
//   }

//   search(params: any): Promise<any> {
//     return this.restangular.one('complains').get(params).toPromise();
//   }

//   findOne(id): Promise<any> {
//     return this.restangular.one('complains', id).get().toPromise();
//   }

//   remove(id): Promise<any> {
//     return this.restangular.one('complains', id).customDELETE().toPromise();
//   }
// }

import { Injectable } from '@angular/core';
import { RestangularModule, Restangular } from 'ngx-restangular';
import 'rxjs/add/operator/toPromise';
import * as _ from 'lodash';
@Injectable()
export class ComplainService {

  private allowFields = [
    'orderId', 'content', 'priority', 'mediaId'
  ]

  private replyAllowFields = [
    'mediaId', 'content'
  ];

  constructor(private restangular: Restangular) { }

  find(params: any): Promise<any> {
    return this.restangular.one('complains').get(params).toPromise();
  }

  search(params: any): Promise<any> {
    return this.restangular.one('complains').get(params).toPromise();
  }

  findOne(id): Promise<any> {
    return this.restangular.one('complains', id).get().toPromise();
  }

  create(data: any): Promise<any> {
    return this.restangular.all('complains').post(_.pick(data, this.allowFields)).toPromise();
  }

  addReply(id, data: any): Promise<any> {
    return this.restangular.one('complains', id).all('conversations').post(_.pick(data, this.replyAllowFields)).toPromise();
  }

  sendRefund(data: any): Promise<any> {
    return this.restangular.all('refundRequests').customPOST(data).toPromise();
  }

  remove(id): Promise<any> {
    return this.restangular.one('complains', id).customDELETE().toPromise();
  }
}
