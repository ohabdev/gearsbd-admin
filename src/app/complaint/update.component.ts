import { Component, OnInit } from '@angular/core';
import { ComplainService } from './service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastyService } from 'ng2-toasty';
import * as _ from 'lodash';

@Component({
  templateUrl: './views/form.html',
  styleUrls: ['./views/form.css']
})
export class ComplaintUpdateComponent implements OnInit {
  public complain: any = {};

  public isSubmitted: any = false;
  public comments: any = [];
  public accessToken: any;
  public reply: any = {
    'content': '',
    'mediaId': null
  };

  constructor(private router: Router, private route: ActivatedRoute, private service: ComplainService, private toasty: ToastyService) {
    const id = this.route.snapshot.params.id;
    this.service.findOne(id).then(resp => {
      this.complain = resp.data;
      this.comments = this.complain.conversationlist;
    }).catch(() => this.toasty.error('Something went wrong, please try again.'));
  }

  ngOnInit() {
  }

  // submit(frm: any) {
  //   if (!frm.valid) {
  //     return this.toasty.error('Something went wrong, please try again.');
  //   }
  //   const data = _.pick(this.complain, ['status', 'note']);
  //   this.service.update(this.complain._id, data)
  //     .then(resp => {
  //       this.toasty.success('Updated successfully!');
  //       this.router.navigate(['/complaints']);
  //     })
  //     .catch(() => alert('Something went wrong, please try again!'));
  // }

  submit(frm: any) {
    this.isSubmitted = true;
    if (frm.invalid) {
      return this.toasty.error('Form is invalid, please try again.');
    }
    this.service.addReply(this.complain._id, this.reply)
      .then((res) => {
        console.log(res);
        this.comments.push(res.data);
        this.complain.updatedAt = res.data.complain.updatedAt;
        this.reply.content = '';
        this.isSubmitted = false;
        this.toasty.success('Replly added!');
      }, err => this.toasty.error(err.data.message || 'Something went wrong!'));
  }
}
