import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SortablejsModule } from 'angular-sortablejs';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { MediaModule } from '../../media/media.module';
import { ProductCategoryService } from './../services/category.service';

import { BoilerplateRoutingModule } from './boilerplate.routing';
import { ListComponent } from './list/list.component';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';

import { BoilerplateService } from "./services/boilerplate.service";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SortablejsModule,
    //our custom module
    BoilerplateRoutingModule,
    NgbModule.forRoot(),
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot(),
    MediaModule
  ],
  declarations: [
    ListComponent,
    CreateComponent,
    EditComponent
  ],
  providers: [
    BoilerplateService,
    ProductCategoryService
  ]
})
export class BoilerplateModule { }
