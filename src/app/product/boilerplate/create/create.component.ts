import { Component, OnInit } from '@angular/core';
import { ToastyService } from 'ng2-toasty';
import { BoilerplateService } from './../services/boilerplate.service';
import { ProductCategoryService } from './../../services/category.service';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  public fileOptions: any = {};
  public media: any;

  public product: any = {
    name: '',
    description: '',
    shortDescription: '',
    mediaId: null,
    categoryId: null,
  };

  public tree: any = [];

  constructor(private router: Router, private boilerService: BoilerplateService, private toasty: ToastyService, private categoryService: ProductCategoryService) {
  }

  ngOnInit() {
    this.categoryService.tree()
      .then(resp => (this.tree = this.categoryService.prettyPrint(resp.data)));
  }

  selectMedia(media: any) {
    this.media = media;
  }

  submit(frm: any) {
    if (frm.invalid) {
      return this.toasty.error('Invalid form, please check again.');
    }

    if (!this.product.name) {
      return this.toasty.error('Please add product name');
    }

    if (this.media) {
      this.product.mediaId = this.media._id;
    } else {
      return this.toasty.error('Please browse product image');
    }

    if (!this.product.categoryId) {
      return this.toasty.error('Please select category');
    }
    this.boilerService.create(this.product)
      .then((resp) => {
        console.log(resp);
        this.toasty.success('Boiler Plate has been created');
        this.router.navigate(['/product-boilerplates']);
      }, err => this.toasty.error(err.data.data.details[0].message || err.data.message || 'Something went wrong!'));
  }

}
