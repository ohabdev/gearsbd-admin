import { Component, OnInit } from '@angular/core';
import { ToastyService } from 'ng2-toasty';
import { BoilerplateService } from './../services/boilerplate.service';
import { ProductCategoryService } from './../../services/category.service';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  public fileOptions: any = {};
  public media: any;

  public product: any = {};

  public tree: any = [];

  constructor(private router: Router, private route: ActivatedRoute, private boilerService: BoilerplateService, private toasty: ToastyService, private categoryService: ProductCategoryService) {
  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.boilerService.findOne(id)
      .then(resp => {
        this.product = resp.data;
        this.media = this.product.mediaId;
      });

    this.categoryService.tree()
      .then(resp => (this.tree = this.categoryService.prettyPrint(resp.data)));
  }

  selectMedia(media: any) {
    this.media = media;
  }

  submit(frm: any) {
    if (frm.invalid) {
      return this.toasty.error('Invalid form, please check again.');
    }

    if (!this.product.name) {
      return this.toasty.error('Please add product name');
    }

    if (this.media) {
      this.product.mediaId = this.media._id;
    } else {
      return this.toasty.error('Please browse product image');
    }

    if (!this.product.categoryId) {
      return this.toasty.error('Please select category');
    }

    this.boilerService.update(this.product._id, this.product).then(resp => {
      this.toasty.success('Updated successfully.');
      this.router.navigate(['/product-boilerplates']);
    });
  }

}
