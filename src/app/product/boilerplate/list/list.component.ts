import { Component, OnInit } from '@angular/core';
import { ToastyService } from 'ng2-toasty';
import { BoilerplateService } from './../services/boilerplate.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  public items = [];
  public page: any = 1;
  public total: any = 0;
  public searchText: any = '';
  public sortOption = {
    sortBy: 'createdAt',
    sortType: 'desc'
  };

  constructor(private router: Router, private boilerService: BoilerplateService, private toasty: ToastyService) {
  }

  ngOnInit() {
    this.query();
  }

  sortBy(field: string, type: string) {
    this.sortOption.sortBy = field;
    this.sortOption.sortType = type;
    this.query();
  }

  query() {
    // console.log({
    //   page: this.page,
    //   q: this.searchText,
    //   sort: `${this.sortOption.sortBy}`,
    //   sortType: `${this.sortOption.sortType}`
    // });

    this.boilerService.search({
      page: this.page,
      q: this.searchText,
      sort: `${this.sortOption.sortBy}`,
      sortType: `${this.sortOption.sortType}`
    })
      .then(resp => {
        // console.log(resp.data);
        this.items = resp.data.items;
        this.total = resp.data.count;
        this.searchText = '';
      })
      .catch(() => this.toasty.error('Something went wrong, please try again!'));
  }

  keyPress(event: any) {
    if (event.charCode === 13) {
      this.query();
    }
  }

  remove(itemId: any, index: number) {
    if (window.confirm('Are you sure want to delete this item?')) {
      this.boilerService.remove(itemId)
        .then(() => {
          this.toasty.success('Item has been deleted!');
          this.items.splice(index, 1);
          this.total--;
        })
        .catch((err) => this.toasty.error(err.data.message || 'Something went wrong, please try again!'));
    }
  }

}
