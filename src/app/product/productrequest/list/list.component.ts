import { Component, OnInit } from "@angular/core";
import { ToastyService } from "ng2-toasty";
import { ProductRequestService } from "../services/productrequest.service";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-list",
  templateUrl: "./list.component.html",
  styleUrls: ["./list.component.css"],
})
export class ListComponent implements OnInit {
  public items: any = [];
  public page: any = 1;
  public total: any = 0;
  public searchText: any = "";
  public sortOption = {
    sortBy: "createdAt",
    sortType: "desc",
  };

  constructor(
    private router: Router,
    private productRequestService: ProductRequestService,
    private toasty: ToastyService
  ) {}

  ngOnInit() {
    this.query();
  }

  query() {
    this.productRequestService
      .search({
        page: this.page,
        q: this.searchText,
        sort: `${this.sortOption.sortBy}`,
        sortType: `${this.sortOption.sortType}`,
      })
      .then((resp) => {
        // console.log(resp.data);
        this.items = resp.data.items;
        this.total = resp.data.count;
        this.searchText = "";
      })
      .catch(() =>
        this.toasty.error("Something went wrong, please try again!")
      );
  }

  keyPress(event: any) {
    if (event.charCode === 13) {
      this.query();
    }
  }

  sortBy(field: string, type: string) {
    this.sortOption.sortBy = field;
    this.sortOption.sortType = type;
    this.query();
  }

  // approve(itemId: any, index: number) {
  //   if (window.confirm("Are you sure want to approve this item?")) {
  //     this.productRequestService
  //       .approve(itemId)
  //       .then(() => {
  //         this.toasty.success("Product has been approved!");
  //         this.items.splice(index, 1);
  //         this.total--;
  //       })
  //       .catch((err) =>
  //         this.toasty.error(
  //           err.data.message || "Something went wrong, please try again!"
  //         )
  //       );
  //   }
  // }
  approve(itemId, index) {
    if (window.confirm("Are you sure want to approve this item?")) {
      const data = { status: "approved" };
      this.productRequestService
        .update(itemId, data)
        .then((resp) => {
          this.toasty.success("Approved successfuly!");
          console.log(data.status);
          this.items.status = data.status;
          this.ngOnInit();
        })
        .catch((err) =>
          this.toasty.error("Something went wrong, please try again!")
        );
    }
  }

  remove(itemId: any, index: number) {
    if (window.confirm("Are you sure want to delete this item?")) {
      this.productRequestService
        .remove(itemId)
        .then(() => {
          this.toasty.success("Product has been deleted!");
          this.items.splice(index, 1);
          this.total--;
        })
        .catch((err) =>
          this.toasty.error(
            err.data.message || "Something went wrong, please try again!"
          )
        );
    }
  }
}
