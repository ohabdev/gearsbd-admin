import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CreateComponent } from "./create/create.component";
import { EditComponent } from "./edit/edit.component";
import { ListComponent } from "./list/list.component";

const routes: Routes = [
  {
    path: "",
    component: ListComponent,
    data: {
      title: "Requested Boilerplates List",
      urls: [
        { title: "Boilerplates Request", url: "/products-request" },
        { title: "Manage Boilerplates" },
      ],
    },
  },

  // {
  //   path: "create",
  //   component: CreateComponent,
  //   data: {
  //     title: "Boilerplates Create",
  //     urls: [
  //       { title: "Boilerplates", url: "/product-boilerplates" },
  //       { title: "Create Boilerplates" },
  //     ],
  //   },
  // },

  // {
  //   path: "update/:id",
  //   component: EditComponent,
  //   data: {
  //     title: "Boilerplates Update",
  //     urls: [
  //       { title: "Boilerplates", url: "/product-boilerplates" },
  //       { title: "Update Boilerplates" },
  //     ],
  //   },
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductRequestRoutingModule { }
