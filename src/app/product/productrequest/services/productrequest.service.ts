import { Injectable } from "@angular/core";
import { RestangularModule, Restangular } from "ngx-restangular";
import "rxjs/add/operator/toPromise";
import * as _ from "lodash";

@Injectable()
export class ProductRequestService {
  private allowFields = [
    "name",
    "description",
    "shortDescription",
    "categoryId",
    "mediaId",
  ];
  private status = ["status"];

  constructor(private restangular: Restangular) {}

  create(data: any): Promise<any> {
    return this.restangular
      .all("boilerplate")
      .post(_.pick(data, this.allowFields))
      .toPromise();
  }

  search(params: any): Promise<any> {
    return this.restangular
      .one("products/request/", "list")
      .get(params)
      .toPromise();
  }

  findOne(id): Promise<any> {
    return this.restangular.one("boilerplate", id).get().toPromise();
  }

  // update(id, data): Promise<any> {
  //   return this.restangular
  //     .one("boilerplate", id)
  //     .customPUT(_.pick(data, this.allowFields))
  //     .toPromise();
  // }

  update(id, data): Promise<any> {
    let r = this.restangular
      .one("products")
      .one("request", id)
      .one("status")
      .customPUT(data)
      .toPromise();
    console.log(r);
    return r;
  }

  // approve(id): Promise<any> {
  //   let data = { status: "approved" };
  //   let r = this.restangular
  //     .one("products")
  //     .one("request", id)
  //     .one("status")
  //     .post(_.pick(data, this.status))
  //     .toPromise();
  //   console.log(r);

  //   return r;
  // }

  remove(id): Promise<any> {
    return this.restangular
      .one("products/request/", id)
      .customDELETE()
      .toPromise();
  }
}
