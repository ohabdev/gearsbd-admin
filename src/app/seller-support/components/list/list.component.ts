import { Component, OnInit } from '@angular/core';
import { ToastyService } from 'ng2-toasty';
import { Router, ActivatedRoute } from '@angular/router';
import { SupportService } from './../../services/support.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  public tickets = [];
  public page: any = 1;
  public total: any = 0;
  public searchText: any = '';
  public take: Number = 10;
  public sortOption = {
    sortBy: 'createdAt',
    sortType: 'desc'
  };

  constructor(private router: Router, private supportService: SupportService, private toasty: ToastyService) { }

  ngOnInit() {
    this.query();
  }

  query() {
    const params = Object.assign({
      page: this.page,
      take: this.take,
      sort: `${this.sortOption.sortBy}`,
      sortType: `${this.sortOption.sortType}`,
      searchText: this.searchText,
    });

    this.supportService.find(params).then((res) => {
      this.tickets = res.data.items;
      this.total = res.data.count;
      // console.log(this.tickets);
    }).catch(() => this.toasty.error('Something went wrong, please try again!'));
  }

  keyPress(event: any) {
    if (event.charCode === 13) {
      this.query();
    }
  }

}
