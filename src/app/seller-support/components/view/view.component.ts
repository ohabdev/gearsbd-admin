import { Component, OnInit } from '@angular/core';
import { SupportService } from '../../services/support.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastyService } from 'ng2-toasty';
import * as _ from 'lodash';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {

  public ticket: any = {};
  public comments: any = [];

  public reply: any = {
    'description': '',
    // 'attachment' : null,
  };

  public isSubmitted: any = false;

  constructor(private router: Router, private route: ActivatedRoute,
    private supportService: SupportService, private toasty: ToastyService) {
    const id = this.route.snapshot.params.id;
    // console.log(id);

    this.supportService.findOne(id).then((res) => {
      // console.log(res);
      // console.log(this.dummyData);
      this.ticket = res.data
      this.comments = this.ticket.comments;
    })
  }

  ngOnInit() {
  }

  submit(frm: any) {
    console.log(frm.controls.description);
    this.isSubmitted = true;
    // console.log(this.reply);
    if (frm.invalid) {
      return this.toasty.error('Form is invalid, please try again.');
    }
    this.supportService.addReply(this.ticket._id, this.reply)
      .then((res) => {
        // console.log(res);
        this.comments.push(res.data);
        this.ticket = res.data.ticket;
        this.reply.description = '';
        this.isSubmitted = false;
        this.toasty.success('Replly added!');
      }, err => this.toasty.error(err.data.message || 'Something went wrong!'));
  }

}
