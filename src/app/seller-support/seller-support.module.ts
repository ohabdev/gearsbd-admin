import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { SellerSupportRoutingModule } from './seller-support.routing';
import { CreateComponent } from './components/create/create.component';
import { EditComponent } from './components/edit/edit.component';
import { ListComponent } from './components/list/list.component';
import { ViewComponent } from './components/view/view.component';

import { SupportService } from './services/support.service';
import { UtilsModule } from '../utils/utils.module';

@NgModule({
  imports: [
    CommonModule,
    SellerSupportRoutingModule,
    FormsModule,
    NgSelectModule,
    NgbModule.forRoot(),
    UtilsModule
  ],
  providers: [
    SupportService,
  ],
  declarations: [CreateComponent, EditComponent, ListComponent, ViewComponent]
})
export class SellerSupportModule { }
