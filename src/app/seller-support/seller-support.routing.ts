import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewComponent } from './components/view/view.component';
import { ListComponent } from './components/list/list.component';

const routes: Routes = [
  {
    path: '',
    component: ListComponent,
    data: {
      title: 'Seller Support',
      urls: [{ title: 'Seller Support', url: '/seller-support' }]
    }
  },
  {
    path: 'view/:id',
    component: ViewComponent,
    data: {
      title: 'Support Ticket Details',
      urls: [{ title: 'Seller Support', url: '/seller-support' }, { title: 'Seller Support', url: ' / seller-support / view /: id' }]
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SellerSupportRoutingModule { }
